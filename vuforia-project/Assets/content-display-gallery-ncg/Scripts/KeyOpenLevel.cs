﻿using UnityEngine;
using System.Collections;

public class KeyOpenLevel : MonoBehaviour {

	public bool teleported = false; //Boolean for teleportation
	public Teleporter destination; //Variable for the destination game object
	public bool hasKey = false;
	
	void OnTriggerEnter(Collider c)
	{
		
		if (c.gameObject.CompareTag("Key")) 
		{
			
			
			if (!teleported) //If the tagged object called player entered the the collider 
				//then do the following
			{
				
				hasKey = true;
				destination.teleported = true;//If the tagged object enters the trigger then activate
				//Teleport to object position
				//c.gameObject.transform.position = destination.gameObject.transform.position;
				Application.LoadLevel("F1"); //When entered the collider then load this level.
			}
			
			
			else 
			{
				hasKey = false;
				destination.teleported = false;
				
			}
		}
		
		
		
	}
	
	void OnTriggerExit(Collider c)
	{
		
		if (c.CompareTag("Key"))
		{
			
			
			
			//When Tagged Object leaves the collider then no longer activate
			teleported = false;
			hasKey = false;
			
		}
		
	}
}
