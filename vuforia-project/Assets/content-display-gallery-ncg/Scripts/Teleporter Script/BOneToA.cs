﻿using UnityEngine;
using System.Collections;

public class BOneToA : MonoBehaviour {

	
	public bool teleported = false; //Boolean for teleportation
	public Teleporter destination; //Variable for the destination game object
	
	void OnTriggerEnter(Collider c)
	{
		
		if (c.CompareTag("Player")) 
		{
			
			if (!teleported) //If the tagged object called player entered the the collider 
				//then do the following
			{
				destination.teleported = true;//If the tagged object enters the trigger then activate
				//Teleport to object position
				//c.gameObject.transform.position = destination.gameObject.transform.position;
				Application.LoadLevel("A"); //When entered the collider then load this level.
			}
		}
		
	}
	
	void OnTriggerExit(Collider c)
	{
		
		if (c.CompareTag("Player"))
		{
			//When Tagged Object leaves the collider then no longer activate
			teleported = false;
		}
		
	}
}
