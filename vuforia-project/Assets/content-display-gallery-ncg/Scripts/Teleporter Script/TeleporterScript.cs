﻿using UnityEngine;
using System.Collections;

public class TeleporterScript : MonoBehaviour {

	public bool teleported = false;
	public Teleporter destination;
	
	void OnTriggerEnter(Collider c)
	{
		
		if (c.CompareTag("Player"))
		{
			
			if (!teleported)
			{
				destination.teleported = true;
				Application.LoadLevel("B2");
			}
		}
		
	}
	
	void OnTriggerExit(Collider c)
	{
		
		if (c.CompareTag("Player"))
		{
			
			teleported = false;
		}
		
	}



}



