﻿#pragma strict

	var paused : boolean = false;
	
	function Update() 
	{

		if(Input.GetButtonUp("pause"))
		{
		
			if(!paused)
			{
			
				Time.timeScale = 0;
				paused = true;
			
			
			}
			
		else
			{
			
				paused = false;
				Time.timeScale = 1;	
			
			
			}
		
		
		}
		
		

	}