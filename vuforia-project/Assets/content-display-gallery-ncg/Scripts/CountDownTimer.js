﻿#pragma strict

	private var startTime;
	private var restSeconds : float;
	private var roundedRestSeconds : int;
	private var displaySeconds : float;
	private var displayMinutes : float;
	var text : String;
	
	public var countDownSeconds = 60;


	function Awake()
	{

		startTime = Time.time;

	}


	function OnGUI()
	{

		var guiTime = Time.time; //startTime
		
		restSeconds = countDownSeconds - (guiTime);
		
		
		if(restSeconds == 60);
		{
		
			print("One Minute Remaining");
		
		}
		
		if(restSeconds == 0);
		{
		
			print("Times up");
			
			//Application.LoadLevel("Game Over Scene");
		
		}
		
		
		//display the timer
		roundedRestSeconds = Mathf.CeilToInt(restSeconds);
		displaySeconds = roundedRestSeconds % 60;
		displayMinutes = roundedRestSeconds / 60;

		text = String.Format ("{0:00}:{1:00}", displayMinutes, displaySeconds);
		
		GUI.color = Color.red;
		 GUI.Label(Rect(400, 25, 100, 30), text); 
	}