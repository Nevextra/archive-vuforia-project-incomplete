﻿#pragma strict

var isQuit=false;

function OnMouseEnter()//When mouse hovers over the object
{
	 //change text color
	 renderer.material.color=Color.red;
}

function OnMouseExit()//When mouse exits the object
{
	 //change text color
	 renderer.material.color=Color.white;
}

function OnMouseUp() //When the mouse is clicked
{
	 //is this quit
	 if (isQuit==true) {
	 //quit the game
	 Application.Quit();
	 }
	 else 
	 {
		 //load level
		 Application.LoadLevel("B2");
	}
}

function Update()
{
	 //quit game if escape key is pressed
	 if (Input.GetKey(KeyCode.Escape)) 
	 { 
	 	
	 	
	 	Application.Quit();
	 	
	 }
}
