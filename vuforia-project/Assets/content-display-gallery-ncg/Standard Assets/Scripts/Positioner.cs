﻿using UnityEngine;
using System.Collections;

public class Positioner : MonoBehaviour {
	
	public int health = 50;
	public float speed = 0.2f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		Debug.Log (health);
			
		if(Input.GetKey(KeyCode.W)){
		Debug.Log("forward");
		transform.Translate(0f,0f,Time.deltaTime*2.0f);	
		}
		
		if(Input.GetKey(KeyCode.S)){
		Debug.Log("backward");	
		transform.Translate (0f,0f,-Time.deltaTime*2.0f);	
		}
		
		if(Input.GetKey(KeyCode.A)){
		Debug.Log("left");	
		transform.Translate (-Time.deltaTime*2.0f,0f,0f);	
		}
		
		if(Input.GetKey(KeyCode.D)){
		Debug.Log("right");	
		transform.Translate (Time.deltaTime*2.0f,0f,0f);	
		}
		
		if(health<=0){
		Debug.Log ("Death Become It");
		Destroy (gameObject);
		}
		
		AudioSource audio = (AudioSource)GetComponent("AudioSource");
		if (!audio.isPlaying) audio.Play();

	}
	
	void OnMouseDown(){
		health=health-10;
		Component cmp=GameObject.FindWithTag("healthtext").GetComponent("TextMesh");
		(cmp as TextMesh).text="Health: "+health;
	}
}
