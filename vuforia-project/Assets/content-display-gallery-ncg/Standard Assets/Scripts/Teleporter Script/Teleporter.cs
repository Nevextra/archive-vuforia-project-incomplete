using UnityEngine;

//Instructions, how to use this code.
//create two boxes and add box collider and check is trigger
//Then Attach this script.
//In the destination box pick an object to teleport to.


public class Teleporter : MonoBehaviour
{

    public bool teleported = false; //Boolean for teleportation
    public Teleporter destination; //Variable for the destination game object
	public bool hasKey = false;

    void OnTriggerEnter(Collider c)
    {

		Debug.Log("Entered key");

		if (c.gameObject.CompareTag("Key")) 
        {
			Debug.Log("Entered key");

            if (!teleported) //If the tagged object called player entered the the collider 
				//then do the following
            {

				Debug.Log("Entered key");
                destination.teleported = true;//If the tagged object enters the trigger then activate
				//Teleport to object position
				//c.gameObject.transform.position = destination.gameObject.transform.position;
				Application.LoadLevel("F1"); //When entered the collider then load this level.
            }
        }

    }

    void OnTriggerExit(Collider c)
    {

		if (c.CompareTag("Key"))
        {
			//When Tagged Object leaves the collider then no longer activate
            teleported = false;
			hasKey = false;
        }

    }

}